#include <stdio.h>

void usage()
{
    printf("Hex2Bin - Shows the binary representation of a hexadecimal value\n");
}

int main (int argc, char ** argv)
{
    int in;
    int i;
    int j;
  
    if(argc == 1)
    {
        usage();
        return 1;
    }

    printf("\n");

    for (j = 1; j < argc; j++) {

        sscanf(argv[j], "%x", &in);

        printf("0x%08x: ", in);

        for (i = 31; i >= 0; i--) {
            if (in & (1 << i))
                putchar('1');
            else
                putchar('0');

            if (!(i % 4))
                putchar(' ');

            if (i == 16)
                printf("- ");
        }

        printf("\n");
    }

    return 0;
}